<?php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Main
 *
 * @ORM\Table(name="main")
 * @ORM\Entity(repositoryClass="LL\GameBundle\Entity\MainRepository")
 */
class Main
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", unique = true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var int
     *
     * @ORM\Column(name="idTour", type="integer")
     */
     private $idTour;

    /**
     * @var int
     *
     * @ORM\Column(name="idManche", type="integer")
     */
     private $idManche;

     /**
     * @var string
     *
     * @ORM\Column(name="pJoueur", type="string", length=255)
     */
     private $pJoueur;

     /**
    * @ORM\ManyToMany(targetEntity="LL\GameBundle\Entity\Carte", cascade={"persist"})
    */
     private $cartes;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add carte
     *
     * @param \LL\GameBundle\Entity\Carte $carte
     *
     * @return Main
     */
    public function addCarte(\LL\GameBundle\Entity\Carte $carte)
    {
        $this->cartes[] = $carte;

        return $this;
    }

    /**
     * Remove carte
     *
     * @param \LL\GameBundle\Entity\Carte $carte
     */
    public function removeCarte(\LL\GameBundle\Entity\Carte $carte)
    {
        $this->cartes->removeElement($carte);
    }

    /**
     * Remove all
     *
     */
    public function removeAllCartes()
    {
        foreach ($this->cartes as $carte) {
            $this->cartes->removeElement($carte);
        }
    }

    /**
     * Get cartes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartes()
    {
        return $this->cartes;
    }

    /**
     * Set idTour
     *
     * @param integer $idTour
     *
     * @return Main
     */
    public function setIdTour($idTour)
    {
        $this->idTour = $idTour;

        return $this;
    }

    /**
     * Get idTour
     *
     * @return integer
     */
    public function getIdTour()
    {
        return $this->idTour;
    }

    /**
     * Set idManche
     *
     * @param integer $idManche
     *
     * @return Main
     */
    public function setIdManche($idManche)
    {
        $this->idManche = $idManche;

        return $this;
    }

    /**
     * Get idManche
     *
     * @return integer
     */
    public function getIdManche()
    {
        return $this->idManche;
    }

    /**
     * Set pJoueur
     *
     * @param string $pJoueur
     *
     * @return Main
     */
    public function setPJoueur($pJoueur)
    {
        $this->pJoueur = $pJoueur;

        return $this;
    }

    /**
     * Get pJoueur
     *
     * @return string
     */
    public function getPJoueur()
    {
        return $this->pJoueur;
    }
}
