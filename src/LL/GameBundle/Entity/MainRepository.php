<?php
// src/LL/GameBundle/Entity/MainRepository.php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class MainRepository extends EntityRepository
{
  public function getMainJ($m,$t,$jp)
  {
    /*$qb = $this->createQueryBuilder('m');

    // On fait une jointure avec l'entité Tour avec pour alias « t »
    $qb
      ->innerJoin('m.tour', 't')
      ->addSelect('t')
    ;

    // Puis on filtre sur l'id
    $qb->where($qb->expr()->in('t.id', $t));

    // On fait une jointure avec l'entité Manche avec pour alias « ma »
    $qb
      ->innerJoin('m.manche', 'ma')
      ->addSelect('ma')
    ;

    // Puis on filtre sur l'id'
    $qb->where($qb->expr()->in('ma.id', $m));
     
     // On fait une jointure avec l'entité joueur avec pour alias « j »
    $qb
      ->innerJoin('m.joueur', 'j')
      ->addSelect('j')
    ;


    // Puis on filtre sur le pseudo du joueur
    $qb->where($qb->expr()->in('j.pseudo', $j));*/

    $query2 = $this->_em->createQuery('SELECT m FROM LLGameBundle:Main m JOIN m.manche mn WHERE mn.id = :ma JOIN m.tour t WHERE t.id = :tr JOIN m.joueur  j  WHERE j.pseudo = :jp');
    $query2->setParameter('tr', $t);
    $query2->setParameter('ma', $m);
    $query2->setParameter('jp', $jp);
    
    // On récupère la Query à partir du QueryBuilder
   // $query = $qb->getQuery();

    // On récupère les résultats à partir de la Query
    $results = $query2->getResult();

    // On retourne ces résultats
    return $results;
  }
}