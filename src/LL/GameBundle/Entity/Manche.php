<?php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Manche
 *
 * @ORM\Table(name="manche")
 * @ORM\Entity(repositoryClass="LL\GameBundle\Repository\MancheRepository")
 */
class Manche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", unique = true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="LL\GameBundle\Entity\Pioche", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $pioche;

    /**
     * @ORM\ManyToOne(targetEntity="LL\GameBundle\Entity\Partie", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $partie;

    /**
    * @ORM\ManyToMany(targetEntity="LL\GameBundle\Entity\Joueur", cascade={"persist"})
    */
     private $joueurs;

     /**
     * @var int
     *
     * @ORM\Column(name="nb_joueur", type="integer")
     */
    private $nbj;

    /**
     * @var string
     *
     * @ORM\Column(name="pGagnant", type="string", length=255)
     */
     private $pGagnant;

    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pioche
     *
     * @param \LL\GameBundle\Entity\Pioche $pioche
     *
     * @return Manche
     */
    public function setPioche(\LL\GameBundle\Entity\Pioche $pioche)
    {
        $this->pioche = $pioche;

        return $this;
    }

    /**
     * Get pioche
     *
     * @return \LL\GameBundle\Entity\Pioche
     */
    public function getPioche()
    {
        return $this->pioche;
    }

    /**
     * Set partie
     *
     * @param \LL\GameBundle\Entity\Partie $partie
     *
     * @return Manche
     */
    public function setPartie(\LL\GameBundle\Entity\Partie $partie)
    {
        $this->partie = $partie;

        return $this;
    }

    /**
     * Get partie
     *
     * @return \LL\GameBundle\Entity\Partie
     */
    public function getPartie()
    {
        return $this->partie;
    }

    

    /**
     * Set pGagnant
     *
     * @param string $pGagnant
     *
     * @return Manche
     */
    public function setPGagnant($pGagnant = null)
    {
        $this->pGagnant = $pGagnant;

        return $this;
    }

    /**
     * Get pGagnant
     *
     * @return string
     */
    public function getPGagnant()
    {
        return $this->pGagnant;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->joueurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add joueur
     *
     * @param \LL\GameBundle\Entity\Joueur $joueur
     *
     * @return Manche
     */
    public function addJoueur(\LL\GameBundle\Entity\Joueur $joueur)
    {
        $this->joueurs[] = $joueur;

        return $this;
    }

    /**
     * Remove joueur
     *
     * @param \LL\GameBundle\Entity\Joueur $joueur
     */
    public function removeJoueur(\LL\GameBundle\Entity\Joueur $joueur)
    {
        $this->joueurs->removeElement($joueur);
    }

    /**
     * Get joueurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJoueurs()
    {
        return $this->joueurs;
    }

    /**
     * Set nbj
     *
     * @param integer $nbj
     *
     * @return Manche
     */
    public function setNbj($nbj)
    {
        $this->nbj = $nbj;

        return $this;
    }

    /**
     * Get nbj
     *
     * @return integer
     */
    public function getNbj()
    {
        return $this->nbj;
    }
}
