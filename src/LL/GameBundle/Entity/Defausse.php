<?php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Defausse
 *
 * @ORM\Table(name="defausse")
 * @ORM\Entity(repositoryClass="LL\GameBundle\Repository\DefausseRepository")
 */
class Defausse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", unique = true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="LL\GameBundle\Entity\Carte", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $carte;

    /**
     * @var string
     *
     * @ORM\Column(name="joueur", type="string", length=255)
     */
     private $joueur;  


     /**
    * @ORM\ManyToOne(targetEntity="LL\GameBundle\Entity\Manche", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
    * 
    */
     private $manche;

     /**
    * @ORM\ManyToOne(targetEntity="LL\GameBundle\Entity\Tour", cascade={"persist"})
    */
     private $tour;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carte
     *
     * @param \LL\GameBundle\Entity\Carte $carte
     *
     * @return Defausse
     */
    public function setCarte(\LL\GameBundle\Entity\Carte $carte)
    {
        $this->carte = $carte;

        return $this;
    }

    /**
     * Get carte
     *
     * @return \LL\GameBundle\Entity\Carte
     */
    public function getCarte()
    {
        return $this->carte;
    }

   

    /**
     * Set manche
     *
     * @param \LL\GameBundle\Entity\Manche $manche
     *
     * @return Defausse
     */
    public function setManche(\LL\GameBundle\Entity\Manche $manche)
    {
        $this->manche = $manche;

        return $this;
    }

    /**
     * Get manche
     *
     * @return \LL\GameBundle\Entity\Manche
     */
    public function getManche()
    {
        return $this->manche;
    }

    /**
     * Set tour
     *
     * @param \LL\GameBundle\Entity\Tour $tour
     *
     * @return Defausse
     */
    public function setTour(\LL\GameBundle\Entity\Tour $tour = null)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return \LL\GameBundle\Entity\Tour
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * Set joueur
     *
     * @param string $joueur
     *
     * @return Defausse
     */
    public function setJoueur($joueur  = null)
    {
        $this->joueur = $joueur;

        return $this;
    }

    /**
     * Get joueur
     *
     * @return string
     */
    public function getJoueur()
    {
        return $this->joueur;
    }
}
