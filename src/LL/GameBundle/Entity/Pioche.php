<?php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pioche
 *
 * @ORM\Table(name="pioche")
 * @ORM\Entity(repositoryClass="LL\GameBundle\Repository\PiocheRepository")
 */
class Pioche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToMany(targetEntity="LL\GameBundle\Entity\Carte", cascade={"persist"})
    */
     private $cartes;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartes = new \Doctrine\Common\Collections\ArrayCollection();
        $nb_carte = 0;
    }

    /**
     * Add carte
     *
     * @param \LL\GameBundle\Entity\Carte $carte
     *
     * @return Pioche
     */
    public function addCarte(\LL\GameBundle\Entity\Carte $carte)
    {
        $this->cartes[] = $carte;

        return $this;
    }

    /**
     * Remove carte
     *
     * @param \LL\GameBundle\Entity\Carte $carte
     */
    public function removeCarte(\LL\GameBundle\Entity\Carte $carte)
    {
        $this->cartes->removeElement($carte);
    }

    /**
     * Get cartes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartes()
    {
        return $this->cartes;
    }
}
