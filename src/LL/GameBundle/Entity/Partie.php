<?php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partie
 *
 * @ORM\Table(name="partie")
 * @ORM\Entity(repositoryClass="LL\GameBundle\Repository\PartieRepository")
 */
class Partie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="commence", type="boolean")
     */
    private $commence;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_joueur", type="integer")
     */
    private $nbj;

    /**
    * @ORM\ManyToMany(targetEntity="LL\GameBundle\Entity\Joueur", cascade={"persist"})
    */
     private $joueurs;

     /**
    * @ORM\ManyToOne(targetEntity="LL\GameBundle\Entity\Joueur", cascade={"persist"})
    * 
    */
     private $gagnant;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->joueurs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commence = false;
        $this->nbj = 0;
    }

    /**
     * Add joueur
     *
     * @param \LL\GameBundle\Entity\Joueur $joueur
     *
     * @return Partie
     */
    public function addJoueur(\LL\GameBundle\Entity\Joueur $joueur)
    {
        $this->joueurs[] = $joueur;

        return $this;
    }

    /**
     * Remove joueur
     *
     * @param \LL\GameBundle\Entity\Joueur $joueur
     */
    public function removeJoueur(\LL\GameBundle\Entity\Joueur $joueur)
    {
        $this->joueurs->removeElement($joueur);
    }

    /**
     * Get joueurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJoueurs()
    {
        return $this->joueurs;
    }

    /**
     * Set gagnant
     *
     * @param \LL\GameBundle\Entity\Joueur $gagnant
     *
     * @return Partie
     */
    public function setGagnant(\LL\GameBundle\Entity\Joueur $gagnant = null)
    {
        $this->gagnant = $gagnant;

        return $this;
    }

    /**
     * Get gagnant
     *
     * @return \LL\GameBundle\Entity\Joueur
     */
    public function getGagnant()
    {
        return $this->gagnant;
    }

    /**
     * Set commence
     *
     * @param boolean $commence
     *
     * @return Partie
     */
    public function setCommence($commence)
    {
        $this->commence = $commence;

        return $this;
    }

    /**
     * Get commence
     *
     * @return boolean
     */
    public function getCommence()
    {
        return $this->commence;
    }

    /**
     * Set nbj
     *
     * @param integer $nbj
     *
     * @return Partie
     */
    public function setNbj($nbj)
    {
        $this->nbj = $nbj;

        return $this;
    }

    /**
     * Get nbj
     *
     * @return integer
     */
    public function getNbj()
    {
        return $this->nbj;
    }
}
