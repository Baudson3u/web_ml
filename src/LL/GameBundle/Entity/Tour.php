<?php

namespace LL\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tour
 *
 * @ORM\Table(name="tour")
 * @ORM\Entity(repositoryClass="LL\GameBundle\Repository\TourRepository")
 */
class Tour
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", unique = true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
    * @ORM\ManyToOne(targetEntity="LL\GameBundle\Entity\Manche", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
    * 
    */
     private $manche;

     /**
    * @ORM\ManyToMany(targetEntity="LL\GameBundle\Entity\Joueur", cascade={"persist"})
    */
     private $immunisés;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set manche
     *
     * @param \LL\GameBundle\Entity\Manche $manche
     *
     * @return Tour
     */
    public function setManche(\LL\GameBundle\Entity\Manche $manche)
    {
        $this->manche = $manche;

        return $this;
    }

    /**
     * Get manche
     *
     * @return \LL\GameBundle\Entity\Manche
     */
    public function getManche()
    {
        return $this->manche;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->immunisés = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add immunisé
     *
     * @param \LL\GameBundle\Entity\Joueur $immunisé
     *
     * @return Tour
     */
    public function addImmunisé(\LL\GameBundle\Entity\Joueur $immunisé)
    {
        $this->immunisés[] = $immunisé;

        return $this;
    }

    /**
     * Remove immunisé
     *
     * @param \LL\GameBundle\Entity\Joueur $immunisé
     */
    public function removeImmunisé(\LL\GameBundle\Entity\Joueur $immunisé)
    {
        $this->immunisés->removeElement($immunisé);
    }

    /**
     * Get immunisés
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImmunisés()
    {
        return $this->immunisés;
    }
}
