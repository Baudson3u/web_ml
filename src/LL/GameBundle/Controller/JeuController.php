<?php

namespace LL\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use LL\GameBundle\Entity\Partie;
use LL\GameBundle\Entity\Joueur;
use LL\GameBundle\Entity\Utilisateur;


class JeuController extends Controller
{
    public function serveurPartiesAction()
    {

        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Partie')
        ;

        $listParties = $repository->findAll();
        
        return $this->render('LLGameBundle:Jeu:ServeurParties.html.twig',array('listParties' => $listParties
        ));
    }

    public function newGameAction(Request $request)
    {
        

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();

        $partie = new Partie();

        //création fromulaire
        // On crée le FormBuilder grâce au service form factory
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $partie);

        // On ajoute les champs de l'entité que l'on veut à notre formulaire
        $formBuilder
          ->add('nbj',      IntegerType::class)
          ->add('save',      SubmitType::class)
        ;


        // À partir du formBuilder, on génère le formulaire
        $form = $formBuilder->getForm();

        // Si la requête est en POST
         if ($request->isMethod('POST')) {
          // On fait le lien Requête <-> Formulaire
          // À partir de maintenant, la variable $advert contient les valeurs entrées dans le formulaire par le visiteur
          $form->handleRequest($request);

          // On vérifie que les valeurs entrées sont correctes
          // (Nous verrons la validation des objets en détail dans le prochain chapitre)
          if ($form->isValid()) {
            if($partie->getNbj() <2){
                $partie->setNbj(2);
            }elseif($partie->getNbj() >4){
                $partie->setNbj(4);
            }
            $em->persist($partie);
            $em->flush();

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirectToRoute('ll_game_salon', array ("id" => $partie->getId(),));
          }
        }

       
        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $em->flush();
        
        return $this->render('LLGameBundle:Jeu:NewGame.html.twig', array("id" => $partie->getId(), 'form' => $form->createView(),
        ));
    }

    public function salonAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Partie')
        ;

         $partie = $repository->find($id);

         
        $this->GenererPartie($partie);

         $joueurs = $partie->getJoueurs();

        return $this->render('LLGameBundle:Jeu:Salon.html.twig', array('id' => $id, 'joueurs' => $joueurs 
        ));
    }

     public function GenererPartie($partie){

        //récupération du nombre de joueurs
        $nbj = $partie->getNbj();

        //création des joueurs
        $joueur1 = new Joueur;
        $joueur1->setPseudo("1");
        $partie->addjoueur($joueur1) ;

        for($i = 2; $i <= $nbj; $i++){
            $joueur = new Joueur;
            $joueur->setPseudo("" +$i);
            $partie->addjoueur($joueur) ;
        }
        
        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        
        // Étape 1 : On « persiste » l'entité
        $em->persist($partie);
        
       
        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $em->flush();
    }

}
