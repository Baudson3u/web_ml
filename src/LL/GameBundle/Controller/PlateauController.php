<?php

namespace LL\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use LL\GameBundle\Entity\Partie;
use LL\GameBundle\Entity\Joueur;
use LL\GameBundle\Entity\Manche;
use LL\GameBundle\Entity\Main;
use LL\GameBundle\Entity\Tour;
use LL\GameBundle\Entity\Pioche;
use LL\GameBundle\Entity\Defausse;



class PlateauController extends Controller
{
    public function indexAction($idP){

        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Partie');
     
        $partie = $repository->find($idP);

        $manche = $this->DebutManche($partie);
        $this->InitDefausse($manche);

        $tour = $this->DistributionCartes($manche);


        $repM = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Main');

        $mainj1 = $repM->findOneBy( array('idTour' => $tour->getId() , 'idManche' => $manche->getId(), 'pJoueur' => '1'));
        $cartes = $mainj1->getCartes();

        return $this->render('LLGameBundle:Plateau:base.html.twig', array("idM" => $manche->getId(), "idT" => $tour->getId(), "joueur" => 1, "cartes" => $cartes, "nbj" => $manche->getNbj()));
    }

    public function choixAction($idM,$idT,$nJoueur){
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Manche');

        $manche = $repository->find($idM);
        $partie = $manche->getPartie();

        $cartes = $manche->getPioche()->getCartes();

        if( $cartes[0] == null || $manche->getNbj() == 1){
            $cartes = $this->MancheFinie($manche,$idT);
            $js = $manche->getJoueurs();
            $joueurs = array();
            $i =0;
            foreach ($js as $j) {
                $joueurs[$i]= $j->getPseudo();
                $i++;
            }
            $pg = $manche->getPGagnant();
            if($this->estFinie($partie) == 0){
                return $this->render('LLGameBundle:Plateau:finPartie.html.twig', array("idP" => $partie->getId(), "gagnant" => $partie->getGagnant()->getPseudo(), "gagnantM" => $pg,"joueurs" => $joueurs, "cartes" => $cartes));
            }else{
                return $this->render('LLGameBundle:Plateau:finManche.html.twig', array("idP" => $partie->getId(), "gagnant" => $pg,"joueurs" => $joueurs, "cartes" => $cartes));
            }
        }else{
     

            $main = $this->Piocher($idM,$idT,$nJoueur);
            $cartes = $main->getCartes();

            return $this->render('LLGameBundle:Plateau:choix.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "cartes" => $cartes, "nbj" => (string)$manche->getNbj()));
        }
    }

    public function mainJAction($idM,$idT,$nJoueurAv, $nC){
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Manche');

        $manche = $repository->find($idM);
        $partie = $manche->getPartie();

        if($nC != 0){
            $this->AChoisi($idM,$idT,$nJoueurAv, $nC);
        }

        $res = $this->JoueurSuivant($manche,$partie,$nJoueurAv);
        $nJoueur = $res[0];
        $nouv = $res[1]; 
        if($nouv == 0){
            $idT = $this->NouveauTour($idT,$manche, $partie);
        }

        $repM = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Main');

        $main = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));
        $cartes = $main->getCartes();

        return $this->render('LLGameBundle:Plateau:base.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "cartes" => $cartes, "nbj" => $manche->getNbj()));
    }

     public function JoueurSuivant($manche,$partie,$nJoueurAv){
        $js = $manche->getJoueurs();
        $fini = 1;
        $trouve = 1;
        $nJ = $nJoueurAv;
        while($nJ <  $partie->getNbj() && $trouve == 1){
            $nJ++;
            $i = 0;
            while ( $i < $manche->getNbj() && $trouve == 1) {
                if($js[$i]->getPseudo() == $nJ ){
                    $trouve = 0;
                }
                $i++;
            }
        }
        $nouv = 1;
        if($trouve == 1){
            $nouv = 0;
            $nJ = $js[0]->getPseudo();
            
        }

        return array($nJ, $nouv);
    }
   
       
    public function DebutManche( $partie){
        
        //Création d'une nouvelle manche 
        $manche = new Manche();
        $manche->setPartie($partie);

        $manche->setNbj($partie->getNbj());

        $joueurs = $partie->getJoueurs();

        foreach ($joueurs as $joueur) {
            $manche->addJoueur($joueur);
        }

        //Creation de la pioche
        $pioche = new Pioche();

        $rep = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Carte');

        $cartes = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);

        shuffle($cartes);

        foreach($cartes as $numCarte){
            $c = $rep->find($numCarte);
            $pioche->addCarte($c);
        }

        $manche->setPioche($pioche);



         // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        
        // Étape 1 : On « persiste » l'entité
        $em->persist($manche);
        
        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $em->flush();
        
        return $manche;
    }

    public function InitDefausse($manche){

        /*// On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();

        $pioche = $manche->getPioche();
        $cartes = $pioche->getCartes();
        $em->persist($pioche);
        $nbj = $manche->getNbj();

        $def = new Defausse();
        $em->persist($def);
        $def->setManche($manche);
        $carte = $cartes[0];
        $def->setCarte($carte);
        $pioche->removeCarte($carte);

        $i = 1;
        if($nbj == 2){
            while($i < 4){
                $deff = new Defausse();
                $deff->setManche($manche);
                $carte = $cartes[$i];
                echo "carte".$carte->getID();
                $deff->setCarte($carte);
                $pioche->removeCarte($carte);
                $em->persist($deff);
                $i++;
            }
        }
        $em->flush();*/
    }

     public function DistributionCartes( $manche){
        
        $joueurs = $manche->getJoueurs();
        $pioche = $manche->getPioche();
        $cartes = $pioche->getCartes();

        //création tour
        $tour = new Tour();
        $tour->setManche($manche);

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        
        // Étape 1 : On « persiste »
        $em->persist($tour);
        $em->flush();

        $i = 0;
        foreach ($joueurs as $joueur) {
            $main = new Main();
            $main->setIdTour($tour->getId());
            $main->setIdManche($manche->getId());
            $main->setPJoueur($joueur->getPseudo());
            $carte = $cartes[$i];
            $main->addCarte($carte);
            $pioche->removeCarte($carte);
            $em->persist($main);
            $i++;
        }


        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $em->flush();

        return $tour;
    }

    public function Piocher($idM,$idT,$nJoueur){

        $repMan = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Manche');

        $repMain = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Main');

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();

        $main = $repMain->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));
        $manche = $repMan->find($idM);
        $pioche = $manche->getPioche();
        $cartes = $pioche->getCartes();

        $em->persist($main);
        $em->persist($pioche);

        $carte = $cartes[0];
        $main->addCarte($carte);
        $pioche->removeCarte($carte);
        $em->flush();

        return $main;
    }

    public function AChoisi($idM,$idT,$nJoueur, $nC){


        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();

        $repMain = $em
        ->getRepository('LLGameBundle:Main');

        $main = $repMain->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));

        $cartes = $main->getCartes();
        foreach ($cartes as $carte) {
            if($carte->getId() == $nC){
                $main->removeCarte($carte);
            }
        }

        $em->flush();
    }

    public function NouveauTour($idAT, $manche, $partie){
        
        $joueurs = $partie->getJoueurs();

        //création tour
        $tour = new Tour();
        $tour->setManche($manche);

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        
        // Étape 1 : On « persiste »
        $em->persist($tour);
        $em->flush();

        $repMain = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Main');

        foreach ($joueurs as $joueur) {
            $main = new Main();
            $main->setIdTour($tour->getId());
            $main->setIdManche($manche->getId());
            $main->setPJoueur($joueur->getPseudo());
            $ancMain = $repMain->findOneBy( array('idTour' => $idAT , 'idManche' => $manche->getId(), 'pJoueur' => $joueur->getPseudo()));
            $cartes = $ancMain->getCartes();
            foreach ($cartes as $carte) {
                $main->addCarte($carte);
            }
            $em->persist($main);
        }


        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $em->flush();

        return $tour->getId();
    }

    public function MancheFinie($manche,$idT){
        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $repMain = $em
        ->getRepository('LLGameBundle:Main');

        $js = $manche->getJoueurs();
        $cartes = array();

        if($manche->getNbj() == 1)
        {
            foreach ($js as $j) {
               $manche->setPGagnant($j->getPseudo());
               $main = $repMain->findOneBy( array('idTour' => $idT , 'idManche' => $manche->getId(), 'pJoueur' => $j->getPseudo()));
               $cartes[0] = $main->getCartes();
            }
            foreach ($cartes as $carte) {   
                $cartes[0] = $carte[0];
            }
        }else{


            $mains = array();

            foreach ($js as $key => $j) {
               $mains[$key] = $repMain->findOneBy( array('idTour' => $idT , 'idManche' => $manche->getId(), 'pJoueur' => $j->getPseudo()));
            }

            $joueurs = array();
            $i=0;
            foreach ($mains as $main) {
                $cartes[$i] = $main->getCartes();
                $joueurs[$i] = $main->getPJoueur();
                $i++;
            }

            $i=0;
            $idg = 0;
            $idcg = 0;
            foreach ($cartes as $carte) {   
                $cartes[$i] = $carte[0];
                $id = $carte[0]->getNum();
                if($id > $idcg){
                    $idcg = $id;
                    $idg = $i;
                }
                $i++;
            }

            $manche->setPGagnant($joueurs[$idg]);
        }

        $em->persist($manche);
        $em->flush();
        return $cartes;
    }

    public function EstFinie($partie){


        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();

        $repMa = $em
        ->getRepository('LLGameBundle:Manche');

        $manches = $repMa->findBy( array('partie' => $partie ));

        $gagnants = array(0,0,0,0);
        $i;
        foreach ($manches as $manche) {
            $i =  $manche->getPGagnant();
            $gagnants[($i)-1]++;
        }


        $nbj = $partie->getNbj();
        $fini = 1;
        $j =0;
        foreach ($gagnants as $nbG) {   
            if((($nbG >= 7) && ($nbj == 2)) || (($nbG >= 5) && ($nbj == 3)) || (($nbG >= 2) && ($nbj == 4))){
                $fini = 0;
                $joueurs = $partie->getJoueurs();
                foreach ($joueurs as $jo) {
                    if($jo->getPseudo() == (string)$j){
                        $partie->setGagnant($jo);
                    }
                }
            }
            $j++;
        }

        $em->persist($partie);
        $em->flush();
        return $fini;
    }


}
