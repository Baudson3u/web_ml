<?php

namespace LL\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LLGameBundle:Default:index.html.twig');
    }
}
