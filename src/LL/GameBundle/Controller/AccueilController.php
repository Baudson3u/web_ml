<?php

namespace LL\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


use LL\GameBundle\Entity\Utilisateur;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AccueilController extends Controller
{
    public function pageAcAction()
    {
        return $this->render('LLGameBundle:Accueil:pageAc.html.twig');
    }

    public function toInscAction(Request $request)
    {
    	$utilisateur = new Utilisateur();

    	//création fromulaire
    	// On crée le FormBuilder grâce au service form factory
	    $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $utilisateur);

	    // On ajoute les champs de l'entité que l'on veut à notre formulaire
	    $formBuilder
	      ->add('Pseudo',      TextType::class)
	      ->add('mdp',     PasswordType::class)
		  ->add('save',      SubmitType::class)
	    ;


	    // À partir du formBuilder, on génère le formulaire
    	$form = $formBuilder->getForm();

	    // Si la requête est en POST
		 if ($request->isMethod('POST')) {
	      // On fait le lien Requête <-> Formulaire
	      // À partir de maintenant, la variable $advert contient les valeurs entrées dans le formulaire par le visiteur
	      $form->handleRequest($request);

	      // On vérifie que les valeurs entrées sont correctes
	      // (Nous verrons la validation des objets en détail dans le prochain chapitre)
	      if ($form->isValid()) {
	        // On enregistre notre objet $advert dans la base de données, par exemple
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($utilisateur);
	        $em->flush();

	        // On redirige vers la page de visualisation de l'annonce nouvellement créée
	        return $this->redirectToRoute('ll_game_new');
	      }
	    }

        return $this->render('LLGameBundle:Accueil:inscription.html.twig',array('form' => $form->createView(),
    	));
    }

    public function toCoAction()
    {
        return $this->render('LLGameBundle:Accueil:connexion.html.twig');
    }

    public function newAction()
    {
        return $this->render('LLGameBundle:Accueil:newG.html.twig');
    }

    public function reglesAction()
    {
        return $this->render('LLGameBundle:Accueil:regles.html.twig');
    }

     public function cartesAction()
    {
        return $this->render('LLGameBundle:Accueil:cartes.html.twig');
    }

}
