<?php

namespace LL\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use LL\GameBundle\Entity\Partie;
use LL\GameBundle\Entity\Joueur;
use LL\GameBundle\Entity\Manche;
use LL\GameBundle\Entity\Main;
use LL\GameBundle\Entity\Tour;
use LL\GameBundle\Entity\Pioche;


class EffetController extends Controller
{
    public function effetsAction($idM, $idT, $nJoueur, $idC){

        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Manche');

        $manche = $repository->find($idM);

         $repC = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Carte');

        $carte = $repC->find($idC);

        $reponse;
        switch ($carte->getNum()) {
            case '1':
                $reponse = $this->ChoisirJoueur($manche, $idM, $idT, $nJoueur, $carte);
                break;

            case '2':
                $reponse = $this->ChoisirJoueur($manche, $idM, $idT, $nJoueur, $carte);
                break;

            case '3':
                $reponse = $this->ChoisirJoueur($manche, $idM, $idT, $nJoueur, $carte);
                break;

            case '4':
                $reponse = $this->EffetServante($manche, $idM, $idT, $nJoueur, $carte);
                break;

            case '5':
                if($this->VerifComptesse($idM, $idT, $nJoueur) == 0){
                    $reponse = $this->EffetComptesse($idM, $idT, $nJoueur);

                }else{
                    $reponse = $this->ChoisirJoueur($manche, $idM, $idT, $nJoueur, $carte);
                }
                break;

            case '6':
                if($this->VerifComptesse($idM, $idT, $nJoueur) == 0){
                    $reponse = $this->EffetComptesse($idM, $idT, $nJoueur);

                }else{
                    $reponse = $this->ChoisirJoueur($manche, $idM, $idT, $nJoueur, $carte);
                }
                break;

            case '7':
                return $this->redirectToRoute('ll_game_mainj', array("idM" => $idM, "idT" => $idT,"nJoueurAv" => $nJoueur, "nC" => 2));
                break;

            case '8':
                $reponse = $this->EffetPrincesse($manche, $idM, $idT, $nJoueur, $carte);
                break;
        }

        return $reponse;
    }

     public function apChoixAction($idM, $idT, $nJoueur,$nJoueurChoix, $idC){

        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Manche');

        $manche = $repository->find($idM);
        
        $repC = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Carte');

        $carte = $repC->find($idC);

        $reponse;
        switch ($carte->getNum()) {
            case '1':
                $reponse = $this->ChoixGarde($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '2':
                $reponse = $this->EffetPretre($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '3':
                $reponse = $this->EffetBaron($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '4':
                $reponse = $this->EffetPretre($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '5':
                $reponse = $this->EffetPrince($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '6':
                $reponse = $this->EffetRoi($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '7':
                $reponse = $this->EffetPretre($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;

            case '8':
                $reponse = $this->EffetPretre($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte);
                break;
        }

        return $reponse;
    }

    public function gardeAction($idM,$idT,$nJoueur,$nJoueurChoix,$numCC,$idC){
        return $this->EffetGarde($idM,$idT,$nJoueur,$nJoueurChoix,$numCC,$idC);
    }

    public function ChoixGarde($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte){


        $repC = $this->getDoctrine()->getManager()
        ->getRepository('LLGameBundle:Carte');
        $cartes = $repC->getAllTypes();

        return $this->render('LLGameBundle:Effet:choixG.html.twig', array("idM" => $idM, "idT" => $idT,"joueur" => $nJoueur, "joueurC" => $nJoueurChoix, "numC" => $carte->getId(), "cartes" => $cartes));
    }

    public function ChoisirJoueur($manche, $idM, $idT, $nJoueur, $carte){

        $joueurs = $manche->getJoueurs();

        $repT = $this->getDoctrine()->getManager()
        ->getRepository('LLGameBundle:Tour');

        $tour = $repT->find($idT);
        $immun = $tour->getImmunisés();

        $pjs = array();
        foreach ($joueurs as $key => $joueur) {
            if($carte->getNum() == 5){
                $imm = 1;
                    foreach($immun as $im) {
                        if($im->getPseudo() == $joueur->getPseudo()){
                            $imm = 0;
                        }
                     } 
                    if($imm == 1){
                        $pjs[$key]=$joueur->getPseudo();
                    }
            }else{
                if($joueur->getPseudo() != $nJoueur){
                    $imm = 1;
                    foreach($immun as $im) {
                        if($im->getPseudo() == $joueur->getPseudo()){
                            $imm = 0;
                        }
                     } 
                    if($imm == 1){
                        $pjs[$key]=$joueur->getPseudo();
                    }
                }
            }
        }

        return $this->render('LLGameBundle:Effet:choisirJ.html.twig', array("idM" => $idM, "idT" => $idT,"joueur" => $nJoueur, "joueurs" => $pjs, "numC" => $carte->getId()));
    }

    


    public function EffetPretre($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte){

        $repM = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LLGameBundle:Main');

        $main = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => (string)$nJoueurChoix));
        $cartes = $main->getCartes();

        return $this->render('LLGameBundle:Effet:ePretre.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "joueurC" => $nJoueurChoix,"cj" => 
            $carte->getId(), "carteC" => $cartes[0]));
    }

    public function EffetPrincesse($manche, $idM, $idT, $nJoueur, $carte){

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $em->persist($manche);
        $joueurs = $manche->getJoueurs();

        foreach ($joueurs as $joueur) {
            if($joueur->getPseudo() == (string)$nJoueur){
                $manche->removeJoueur($joueur);
                $manche->setNbj($manche->getNbj()-1);
            }
        }

        $em->flush();

        return $this->render('LLGameBundle:Effet:ePrincesse.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "cj" => 0));
    }

    public function EffetRoi($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte){

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $repM = $em
        ->getRepository('LLGameBundle:Main');

        $mainJC = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueurChoix));
        $mainJ = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));

        $mainJC->setPJoueur($nJoueur);
        $mainJ->setPJoueur($nJoueurChoix);
        $mainJ->removeCarte($carte);
        $cartes = $mainJC->getCartes();

        $em->flush();

        return $this->render('LLGameBundle:Effet:eRoi.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "joueurC" => $nJoueurChoix, "cj" => 0, "carteC" => $cartes[0]));
    }

    public function EffetPrince($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte){

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $repM = $em
        ->getRepository('LLGameBundle:Main');


        $mainJ = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));
        $mainJC = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueurChoix));

        $mainJ->removeCarte($carte);
        $mainJC->removeAllCartes();  
        $pioche = $manche->getPioche();
        $em->persist($pioche);
        $cs = $pioche->getCartes();
        $mainJC->addCarte($cs[0]);
        $pioche->removeCarte($cs[0]);
        $em->flush();
        $cartes = $mainJC->getCartes();
        foreach ($cartes as $cc) {
            $c = $cc;
        }

        return $this->render('LLGameBundle:Effet:ePrince.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "joueurC" => $nJoueurChoix, "cj" => 0, "carteC" => $c));
    }

    public function EffetBaron($manche, $idM, $idT, $nJoueur, $nJoueurChoix, $carte){

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $em->persist($manche);
        $repM = $em
        ->getRepository('LLGameBundle:Main');

        $mainJ = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));
        $mainJC = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueurChoix));
        $mainJ->removeCarte($carte);
        $csJC = $mainJC->getCartes();
        foreach ($csJC as $cc) {
            $cJC = $cc;
        }
        $csJ = $mainJ->getCartes();
        foreach ($csJ as $cc) {
            $cJ = $cc;
        }

        $elimine = 0;
        if($cJ->getNum() < $cJC->getNum()){
            $elimine = $nJoueur;
        }elseif ($cJ->getNum() > $cJC->getNum()) {
            $elimine = $nJoueurChoix;
        }

        if($elimine != 0){
            $joueurs = $manche->getJoueurs();
            foreach ($joueurs as $joueur) {
                if($joueur->getPseudo() == (string)$elimine){
                    $manche->removeJoueur($joueur);
                    $manche->setNbj($manche->getNbj()-1);
                }
            }
        }

        $em->flush();
        return $this->render('LLGameBundle:Effet:eBaron.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "joueurC" => $nJoueurChoix, "elimine" => $elimine, "cj" => 0, "carteC" => $cJC,  "carteJ" => $cJ));
    }

    public function EffetServante($manche, $idM, $idT, $nJoueur, $carte){

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $repT = $em
        ->getRepository('LLGameBundle:Tour');

        $tour = $repT->find($idT);
        $js = $manche->getJoueurs();
        foreach ($js as $j) {
            if($j->getPseudo() == $nJoueur){
                $tour->addImmunisé($j);
            }
        }

        $em->flush();

        return $this->render('LLGameBundle:Effet:eServante.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "cj" => $carte->getId()));
    }

    public function VerifComptesse( $idM, $idT, $nJoueur){

        $repM = $this->getDoctrine()->getManager()
        ->getRepository('LLGameBundle:Main');

        $mainJ = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueur));
        $cs = $mainJ->getCartes();

        $triche = 1;
        foreach ($cs as $c) {
            if($c->getNum() == 7){
                $triche = 0;
            }
        }

        return $triche;
    }

    public function EffetComptesse( $idM, $idT, $nJoueur){

        return $this->render('LLGameBundle:Effet:eComptesse.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "cj" => 2));
    }

    public function EffetGarde($idM,$idT,$nJoueur,$nJoueurChoix,$numCC,$idC){

        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        $repM = $em->getRepository('LLGameBundle:Main');

        $mainJC = $repM->findOneBy( array('idTour' => $idT , 'idManche' => $idM, 'pJoueur' => $nJoueurChoix));
        $juste = 1;
        $csJC = $mainJC->getCartes();
        foreach ($csJC as $cc) {
            if($cc->getNum() == $numCC){
                $juste = 0;
            }
        }
        

        if($juste == 0){   
            $repMn = $em->getRepository('LLGameBundle:Manche');
            $manche = $repMn->find($idM);
            $joueurs = $manche->getJoueurs();

            foreach ($joueurs as $joueur) {
                if($joueur->getPseudo() == (string)$nJoueurChoix){
                    $manche->removeJoueur($joueur);
                    $manche->setNbj($manche->getNbj()-1);
                }
            }
            $em->flush();
        }

        $em->flush();
        return $this->render('LLGameBundle:Effet:eGarde.html.twig', array("idM" => $idM, "idT" => $idT, "joueur" => $nJoueur, "joueurC" => $nJoueurChoix, "juste" => $juste, "cj" => $idC));
    }
}
