<?php
// src/LL/GameBundle/DataFixtures/ORM/LoadCarte.php

namespace LL\GameBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LL\GameBundle\Entity\Carte;

class LoadCarte implements FixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    // Liste des cartes à ajouter
    $names = array(
      1 => 'Princesse',
      2 => 'Comptesse',
      3 => 'Roi',
      4 => 'Prince',
      5 => 'Prince',
      6 => 'Servante',
      7 => 'Servante',
      8 => 'Baron',
      9 => 'Baron',
      10 => 'Pretre',
      11 => 'Pretre',
      12 => 'Garde',
      13 => 'Garde',
      14 => 'Garde',
      15 => 'Garde',
      16 => 'Garde'
    );

    $nums = array(
      1 => 8,
      2 => 7,
      3 => 6,
      4 => 5,
      5 => 5,
      6 => 4,
      7 => 4,
      8 => 3,
      9 => 3,
      10 => 2,
      11 => 2,
      12 => 1,
      13 => 1,
      14 => 1,
      15 => 1,
      16 => 1
    );

     $effets = array(
      1 => 'si vous défaussez cette carte, vous êtes éliminé de la manche',
      2 => ' Si vous avez cette carte en main en même temps que le King ou le Prince, alors vous devez défausser la carte de la Countess',
      3 => 'échangez votre main avec un autre joueur de votre choix',
      4 => ' choisissez un joueur (y compris vous), celui-ci défausse la carte qu il a en main pour en piocher une nouvelle',
      5 => ' choisissez un joueur (y compris vous), celui-ci défausse la carte qu il a en main pour en piocher une nouvelle',
      6 => ' Jusqu au prochain tour, vous êtes protégé des effets des cartes des autres joueurs',
      7 => ' Jusqu au prochain tour, vous êtes protégé des effets des cartes des autres joueurs',
      8 => ' Comparez votre carte avec celle d un autre joueur, celui qui a la carte avec la plus faible valeur est éliminé de la manche',
      9 => ' Comparez votre carte avec celle d un autre joueur, celui qui a la carte avec la plus faible valeur est éliminé de la manche',
      10 => ' Regardez la main d un autre joueur',
      11 => ' Regardez la main d un autre joueur',
      12 => 'Choisissez un joueur et essayez de deviner la carte qu il a en main (excepté le Guard), si vous tombez juste, le joueur est éliminé de la manche',
      13 => 'Choisissez un joueur et essayez de deviner la carte qu il a en main (excepté le Guard), si vous tombez juste, le joueur est éliminé de la manche',
      14 => 'Choisissez un joueur et essayez de deviner la carte qu il a en main (excepté le Guard), si vous tombez juste, le joueur est éliminé de la manche',
      15 => 'Choisissez un joueur et essayez de deviner la carte qu il a en main (excepté le Guard), si vous tombez juste, le joueur est éliminé de la manche',
      16 => 'Choisissez un joueur et essayez de deviner la carte qu il a en main (excepté le Guard), si vous tombez juste, le joueur est éliminé de la manche'
    );


    $images = array(

      1 => 'bundles/llgame/images/princesse.png',
      2 => 'bundles/llgame/images/comptesse.png',
      3 => 'bundles/llgame/images/roi.png',
      4 => 'bundles/llgame/images/prince.png',
      5 => 'bundles/llgame/images/prince.png',
      6 => 'bundles/llgame/images/servante.png',
      7 => 'bundles/llgame/images/servante.png',
      8 => 'bundles/llgame/images/baron.png',
      9 => 'bundles/llgame/images/baron.png',
      10 => 'bundles/llgame/images/pretre.png',
      11 => 'bundles/llgame/images/pretre.png',
      12 => 'bundles/llgame/images/garde.png',
      13 => 'bundles/llgame/images/garde.png',
      14 => 'bundles/llgame/images/garde.png',
      15 => 'bundles/llgame/images/garde.png',
      16 => 'bundles/llgame/images/garde.png'
      );

    for($i = 1; $i <= 16; $i++){

      // On crée la carte
      $carte = new Carte();
      $carte->setNom($names[$i]);
      $carte->setNum($nums[$i]);
      $carte->setEffet($effets[$i]);
      $carte->setImage($images[$i]);


      // On la persiste
      $manager->persist($carte);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}


